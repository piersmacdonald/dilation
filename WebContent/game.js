/*
*
* Global variables (evil)
*
*/
var canvasWidth = 800, //width of the canvas  
  canvasHeight = 500, //height of the canvas  
  canvas = document.getElementById('gameCanvas'), //canvas itself   
  gLoop, //game loop
  //and two-dimensional graphic context of the  
  //canvas, the only one supported by all   
  //browsers for now  
  ctx = canvas.getContext('2d'); 

  //setting canvas size 
  canvas.width = canvasWidth;  
  canvas.height = canvasHeight;  

var STAR = {
  XPOS : {value: 0, name: "x position"}, 
  YPOS: {value: 1, name: "y postion"}, 
  RADIUS : {value: 2, def: 5},
  OPACITY : {value: 3, name: "Opacity"},
  MOVESPEED : {value: 1}
};
  
var PLAYER = {
  PLAYER1 : {value: 0, color: "red"}, 
  PLAYER2: {value: 1, color: "blue"}, 
  AI : {value: 2, color: "green"}
};
 
var BASE = {
  SMALL : {width: 10, height: 10}, 
  MEDIUM: {width: 20, height: 20}, 
  LARGE: {width: 30, height: 30}
};

var click = null;
  
var clear = function(){  
    ctx.fillStyle = '#000000';  
  //set active color to #d0e... (nice blue)  
  //UPDATE - as 'Ped7g' noticed - using clearRect() in here is useless, we cover whole surface of the canvas with blue rectangle two lines below. I just forget to remove that line  
  //ctx.clearRect(0, 0, width, height);  
  //clear whole surface  
    ctx.beginPath();  
  //start drawing  
    ctx.rect(0, 0, canvasWidth, canvasHeight);  
  //draw rectangle from point (0, 0) to  
  //(width, height) covering whole canvas  
    ctx.closePath();  
  //end drawing  
    ctx.fill();  
  //fill rectangle with active  
  //color selected before  
  };  

var howManyStars = 10, circles = [];  

for (var i = 0; i < howManyStars; i++)   
  circles.push([Math.random() * canvasWidth, Math.random() * canvasHeight, Math.random() * STAR.RADIUS.def, Math.random() / 2]);  
//add information about circles into  
//the 'circles' Array. It is x & y positions,   
//radius from 0-100 and transparency   
//from 0-0.5 (0 is invisible, 1 no transparency)  
  
var DrawBackGround = function(){  
  for (var i = 0; i < howManyStars; i++) {  
    ctx.fillStyle = 'rgba(255, 255, 255, ' + circles[i][3] + ')';  
    //white color with transparency in rgba  
    ctx.beginPath();  
    ctx.arc(circles[i][STAR.XPOS.value], circles[i][STAR.YPOS.value], circles[i][STAR.RADIUS.value], 0, Math.PI * 2, true);  
	//arc(x, y, radius, startAngle, endAngle, anticlockwise)  
	//circle has always PI*2 end angle  
    ctx.closePath();  
    ctx.fill();  
  }  
  
  for(var i = 0; i < numBases; i++){
	    var base = bases[i];
	    base.draw(i);
  }
  
}; 


var AnimateBackground = function(deltaY){  
  for (var i = 0; i < howManyStars; i++) {
	//the circle is under the screen so we change  
	//informations about it   
    if (circles[i][STAR.YPOS.value] - circles[i][STAR.RADIUS.value] > canvasHeight) 
    {  
      circles[i][STAR.XPOS.value] = Math.random() * canvasWidth;  
      circles[i][STAR.RADIUS.value] = Math.random() * STAR.RADIUS.def;  
      circles[i][STAR.YPOS.value] = 0 - circles[i][2];  
      circles[i][STAR.OPACITY.value] = Math.random() / 2;  
    } 
    else
    {  
    	//move circle deltaY pixels down  
    	circles[i][STAR.YPOS.value] += deltaY;  
    }  
  }  
};

//***********************************
//***********************************
//*********base related************
//***********************************
//***********************************

function Force(owner, strength){
	this.endPoint = base;
	this.distance = dist;
	
	this.develop = function(num){ 
		
	}
}

function BaseConnection(base, dist){
	this.endPoint = base;
	this.distance = dist;
}

function Base(x, y, size){  

    this.width = size.width, this.height = size.height;  
    this.X = x, this.Y = y;  

    
    //array of all connections for instance of a base
    this.connectedBases = []; 
	this.setConnectedBases = function(connection){
		this.connectedBases.push(connection);
	} 
	  
	this.draw = function(num){  
	        try {
	        	if(click != null){
		        	if((this.X - this.width /2) <= click.x && click.x <= (this.X + this.width /2)){
		        		if((this.Y - this.height /2) <= click.y && click.y <= (this.Y + this.height /2)){
			        		console.log( (this.X - this.width /2) + ' ' + click.x+' '+(this.X + this.width /2));
			        		console.log( (this.Y - this.height /2) + ' ' + click.y+' '+(this.Y + this.height /2));
			        		ctx.fillStyle = 'yellow';
		        		}
		        	}
	        	}else{
	        		ctx.fillStyle = 'grey';
        		}
	        	
	        	//draw base
	        	
	        	ctx.fillRect(this.X - this.width /2, this.Y - this.height / 2, this.width, this.height); 
	        	
	            //ctx.fillStyle = "yellow"; // text color
	            //ctx.fillText(num, this.X,this.Y);
	            
	        	if(this.connectedBases.length != null){
		    		for(var i = 0; i < this.connectedBases.length; i++){
		    			
		    			var cxn = this.connectedBases[i];
	
		    			ctx.moveTo(this.X, this.Y );
		    			ctx.lineTo(bases[cxn.endPoint].X , bases[cxn.endPoint].Y);
		    			ctx.strokeStyle = 'grey'; 
		    			ctx.stroke();
		    		}
	        	}
	        } catch (e) { 
	        	console.log('Base.draw - ' + e.message);
	        }  
	    }  
	};  
	//we immediately execute the function above and   
	//assign its result to the 'player' variable  
	//as a new object   
	  
var numBases = 9, bases = []; 	

bases.push(new Base(200, 100, BASE.MEDIUM));
bases.push(new Base(200, 250, BASE.MEDIUM));
bases.push(new Base(200, 400, BASE.MEDIUM));

bases.push(new Base(400, 100, BASE.MEDIUM));
bases.push(new Base(400, 250, BASE.MEDIUM));
bases.push(new Base(400, 400, BASE.MEDIUM));

bases.push(new Base(600, 100, BASE.MEDIUM));
bases.push(new Base(600, 250, BASE.MEDIUM));
bases.push(new Base(600, 400, BASE.MEDIUM));

bases[4].setConnectedBases(new BaseConnection(1, 3));
bases[4].setConnectedBases(new BaseConnection(3, 5));
/*bases[4].setConnectedBases([1,3,5,7]);
bases[0].setConnectedBases([1,3]);
bases[2].setConnectedBases([1,5]);
bases[6].setConnectedBases([3,7]);
bases[8].setConnectedBases([5,7]);*/

//***********************************
//***********************************
//*********Player related************
//***********************************
//***********************************

function Player(type){  
	
	this.playerType = type;    
	this.width = 50, this.height = 50;  
	
    this.X = 0, this.Y = 0;  
    this.setPosition = function(x, y){  
    	this.X = x;  
    	this.Y = y;  
    } 
    
    this.bases = [];
    this.forces = [];

    this.connectedBases = [];
	//methods   
	this.setConnectedBases = function(cb){
		this.connectedBases = cb;
	} 
	  
	this.draw = function(){  
	        try {          		  
	    		for(var i = 0; i < this.connectedBases.length; i++){
	    			
	    			var subb = bases[this.connectedBases[i]];

	    			ctx.moveTo(this.X, this.Y);
	    			ctx.lineTo(subb.X, subb.Y);
	    			ctx.strokeStyle = 'grey'; 
	    			ctx.stroke();
	    		}
	    		
	        	ctx.fillStyle = PLAYER[this.playerType].color;
	        	ctx.fillRect(this.X -  this.width/2,this.Y - this.height / 2, this.width, this.height); 
	        } catch (e) { 
	        	console.log('Player.draw - ' + e.message);
	        }  
	    }  
	};  
	//we immediately execute the function above and   
	//assign its result to the 'player' variable  
	//as a new object   
	  
var player1 = new Player("PLAYER1");
player1.setPosition(~~(canvasWidth/8),  ~~(canvasHeight /2));
player1.setConnectedBases([0,1,2]);

var player2 = new Player("PLAYER2");
player2.setPosition(~~((canvasWidth/8)*7),  ~~(canvasHeight /2));
player2.setConnectedBases([6,7,8]);
	
//***********************************
//***********************************
//************USER HANDLING**********
//***********************************
//***********************************

ctx.canvas.addEventListener("click", function(e) {
    click = getXandY(e);
}, false);

var getXandY = function(e) {
    var x =  e.clientX - ctx.canvas.getBoundingClientRect().left ;
    var y = e.clientY - ctx.canvas.getBoundingClientRect().top;
    console.log('CLICK - (x:'+x+' y:' + y+')');
    return {x: x, y: y};
}

/*document.onmousemove = function(e){  
    if (player.X + c.offsetLeft > e.pageX) {  
    //if mouse is on the left side of the player.  
    player.moveLeft();  
    } else if (player.X + c.offsetLeft < e.pageX) {  
    //or on right?  
    player.moveRight();  
    }  
    }  */

//**********************************
//***********************************
//************GAME LOOP**************
//***********************************
//***********************************
	
var GameLoop = function(){  
  clear();  
  AnimateBackground(STAR.MOVESPEED.value);  
  DrawBackGround();  
  player1.draw(); 
  player2.draw();

  click = null;
  gLoop = setTimeout(GameLoop, 1000 / 50);  
};  

GameLoop(); 